'''

Name : snake_game.py
Description : Snake game with pygame
Input : Give speed and color 
output : snake game console 
usage : python snake_game.py
example - execute the script - enter input as prompted
    1
    2


author : Abhiyan Timilsina

'''

#Importing necessary packages
import sys, pygame
import random

#Initializing the pygame 
pygame.init()

#Prompting to get difficulty
speed = input("\t\tENTER DIFFICULTY LEVEL 1 2 3\t\t\n")

#Snake color input
color_index = input("\t\tENTER COLOR FOR SNAKE 1: for red 2:for blue and 3 for Green \t\t")

#initializing global variables
level = [20,30,50]
tail_direction = 'RIGHT'
tail_previous_position = [0,0]
new = []
prev_dir = 'RIGHT'
colors = [(255,0,0),(0 , 0, 255),(0,255,0)]
clock = pygame.time.Clock()
black = 0, 0, 0
white = 255,255,255
red = 0,0,255
direction_to_move = 'RIGHT'
food = []

#Giving window size for display
size = width, height = 1024,640

#Initializing snake body 
snake_body = []
snake_length =122
snake_head = [int(size[0]*0.5), int(size[1]*0.2)]
snake_body_radius = 5
snake_direction  = 'RIGHT'
snake_color = colors[int(color_index)-1]

#Setting up main window surface
screen = pygame.display.set_mode(size)

#Function for generating new position 
def generate_new_body_pos(type,towards=False):
    global snake_head
    global snake_direction
    if type == 0:
     snake_body.append(snake_head)
     for i in range(1,snake_length):
       snake_body.append([snake_head[0]-(i*snake_body_radius*2),snake_head[1]])
     tail_previous_position = snake_body[len(snake_body)-1]
    else :
        if towards=='RIGHT' and snake_direction!='LEFT':
            new_head= [snake_head[0]+(snake_body_radius*2),snake_head[1]]
            snake_head = new_head
            snake_body.insert(0,new_head)
            snake_body.pop()
            snake_direction = 'RIGHT'
             
        elif towards=='LEFT' and snake_direction!='RIGHT':
            new_head = [snake_head[0]-(snake_body_radius*2),snake_head[1]]
            snake_head = new_head
            snake_body.insert(0,new_head)
            snake_body.pop()
            snake_direction = 'LEFT'

        elif towards=='UP' and snake_direction!='DOWN':
            new_head = [snake_head[0],snake_head[1]-(snake_body_radius*2)]
            snake_head = new_head
            snake_body.insert(0,new_head)
            snake_body.pop()
            snake_direction = 'UP'
        
        elif towards=='DOWN' and snake_direction != 'UP':
            new_head = [snake_head[0],snake_head[1]+(snake_body_radius*2)]
            snake_head = new_head
            snake_body.insert(0,new_head)
            snake_body.pop()
            snake_direction = 'DOWN'
        else :
            generate_new_body_pos(1,snake_direction)
             

#Detecting Collision in game 
def detect_collision():
    
    if snake_body[0][0]<=3 or snake_body[0][0]>=size[0]-3 or snake_body[0][1]<=3 or snake_body[0][1]>=size[1]-3 :
        print("GAME OVER")
        exit()
    if snake_body[0] in snake_body[1:] :
        print("GAME OVER")
        exit()
    

generate_new_body_pos(0)     

#Draw snake body
def draw_body(snake_body):
    
    global tail_previous_position
    global tail_direction
    for i in range(0,len(snake_body)):
        if i == 0 :
         head_rect = pygame.draw.circle(screen,white,snake_body[i],snake_body_radius,0)
        elif i==len(snake_body)-1 :
            
            tail = snake_body[len(snake_body)-1]
            
           #Moving Right
            if tail_previous_position[1] == tail[1] and tail[0]>tail_previous_position[0] :
               
               tail_direction='RIGHT'
            #MOVING Left
            if tail_previous_position[1] == tail[1] and tail[0]<tail_previous_position[0] :
               
               tail_direction='LEFT'

            if tail_previous_position[0] == tail[0] and tail[1]>tail_previous_position[1] :
               tail_direction='DOWN'
            #MOVING LEFT
            if tail_previous_position[0] == tail[0] and tail[1]<tail_previous_position[1] :
               tail_direction='UP'
            tail_previous_position = tail
            pygame.draw.circle(screen,snake_color,snake_body[i],snake_body_radius,0)  
        else :
          pygame.draw.circle(screen,snake_color,snake_body[i],snake_body_radius,0)

    return head_rect

#Increase the snake
def increase_snake():
    global snake_body
    global snake_length
    snake_length+=1
    new_tail = []
    current_tail = snake_body[len(snake_body)-1]
    if tail_direction == 'RIGHT':
        new_tail = [current_tail[0]-(snake_body_radius*2),current_tail[1]]
    if tail_direction == 'LEFT':
        new_tail = [current_tail[0]+(snake_body_radius*2),current_tail[1]]
    if tail_direction == 'UP':
        new_tail = [current_tail[0]-(snake_body_radius*2),current_tail[1]]
    if tail_direction == 'DOWN':
        new_tail = [current_tail[0]+(snake_body_radius*2),current_tail[1]]     
    snake_body.append(new_tail)

#Main Game Loop
while 1:
    #Getting events fro Events Queue
    for event in pygame.event.get():
        if event.type == pygame.QUIT: pygame.exit()
         
        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            direction_to_move = 'LEFT' 
        elif keys[pygame.K_RIGHT]:
            direction_to_move = 'RIGHT'
        elif keys[pygame.K_UP]:
            direction_to_move = 'UP'
        elif keys[pygame.K_DOWN]:
            direction_to_move = 'DOWN'
    
    
    if new!=snake_body:
        new = snake_body
    
    screen.fill(black)
    font= pygame.font.SysFont(None, 40)
    text =font.render("Snake Size"+str(snake_length),True,white)
    screen.blit(text,(0,0))
    
    generate_new_body_pos(1,direction_to_move)
    
    if not len(food) :
     
     food_x = random.randint(5,size[0]-5)
     food_y = random.randint(5,size[1]-5)
     food = [food_x , food_y]

    #Drawing the snake
    head_rect = draw_body(snake_body) 
    
    #Managing food
    if len(food) :
      food_rect = pygame.draw.rect(screen,(0,255,0),(food_x,food_y,12,12),0)
    
    #Detecting all kinds of collison
    if detect_collision():
        pygame.quit()
        
    if food_rect.colliderect(head_rect):
        food = []
        
        #increase length
        increase_snake();
 
    pygame.display.flip()
    
    #controls the speed fps
    clock.tick(level[int(speed)-1])

